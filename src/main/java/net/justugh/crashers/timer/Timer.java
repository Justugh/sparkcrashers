package net.justugh.crashers.timer;

import lombok.Getter;
import lombok.Setter;
import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.state.GameState;
import net.justugh.crashers.state.GameTask;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.TimeUnit;

@Getter
public class Timer<T extends SparkGame<T, ?>> extends BukkitRunnable {

    private T game;
    private int stateIndex = -1;
    private GameState<T> currentState;

    @Setter
    @Deprecated
    private boolean tickPermanentIfNotRunning = true;

    private long currentTime = 0;
    @Setter
    private boolean running = true;

    public Timer(T game, Plugin plugin) {
        this.game = game;
        if (game.getStates().size() == 0)
            throw new UnsupportedOperationException("Timer can't be run with 0 states");
        runTaskTimer(plugin, 0, 20);
    }

    public GameState<T> getNextState() {
        if (stateIndex + 1 < game.getStates().size()) {
            return game.getStates().get(stateIndex + 1);
        }

        return null;
    }

    public void resetCurrentTime() {
        currentTime = 0;
    }

    public long getRemainingTime() {
        return getRemainingTime(TimeUnit.SECONDS);
    }

    public long getRemainingTime(TimeUnit unit) {
        return unit.convert(currentState.getLength() - currentTime - 1, TimeUnit.SECONDS);
    }

    public boolean hasElapsed(int time) {
        return getRemainingTime() <= (currentState.getLength() - time);
    }

    public boolean isFirstTick() {
        return this.currentTime == 0;
    }

    public void switchState(int index) {
        if (index < game.getStates().size()) {
            GameState<T> state = game.getStates().get(index);
            if (state != null) {
                if (currentState != null)
                    currentState.onEnd(game, this);

                this.currentState = state;
                this.currentTime = 0;
                this.stateIndex = index;
                state.onStart(game, this);
            }
        }
    }

    @Override
    public void run() {
        for (GameTask<T> task : game.getTasks()) {
            task.onTick(game, this);
        }

        if (this.running) {
            if (currentState == null || (!currentState.isInfinite() && currentTime >= currentState.getLength())) {
                switchState(stateIndex + 1);
            }

            if (currentState != null && (!isFirstTick() || !currentState.isIgnoreFirstTick()))
                currentState.onTick(game, this);

            if (currentState != null && !currentState.isInfinite()) {
                currentTime++;
            }
        }
    }
}
