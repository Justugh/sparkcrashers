package net.justugh.crashers.game.impl.state;

import net.justugh.crashers.game.impl.CrystalCrashers;
import net.justugh.crashers.state.GameState;
import net.justugh.crashers.timer.Timer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.TimeUnit;

public class EndState extends GameState<CrystalCrashers> {

    @Override
    public long getLength() {
        return GameState.getLength(30, TimeUnit.SECONDS);
    }

    @Override
    public void onStart(CrystalCrashers game, Timer timer) {

    }

    @Override
    public void onTick(CrystalCrashers game, Timer timer) {
        if (timer.getRemainingTime() % 10 == 0 && timer.getRemainingTime() > 0) {
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("§c§lThe server will close in " + timer.getRemainingTime() + "s");
            Bukkit.broadcastMessage("§7All players will be kicked!");
            Bukkit.broadcastMessage("");
        } else if (timer.getRemainingTime() <= 0) {
            timer.setRunning(false);
            for (Player p : game.getOnlinePlayers()) {
                p.kickPlayer("§cGame Finished!");
            }

            new BukkitRunnable() {
                @Override
                public void run() {
                    Bukkit.getServer().spigot().restart();
                }
            }.runTaskLater(game.getPlugin(), 20 * 10);
        }
    }

    @Override
    public void onEnd(CrystalCrashers game, Timer timer) {

    }

}
