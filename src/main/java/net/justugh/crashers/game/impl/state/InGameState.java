package net.justugh.crashers.game.impl.state;

import lombok.Getter;
import net.justugh.crashers.SparkCrashers;
import net.justugh.crashers.game.impl.CrystalCrashers;
import net.justugh.crashers.game.impl.CrystalPlayer;
import net.justugh.crashers.game.impl.listener.GameListener;
import net.justugh.crashers.state.GameState;
import net.justugh.crashers.state.JoinableState;
import net.justugh.crashers.timer.Timer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.util.concurrent.TimeUnit;

@Getter
public class InGameState extends GameState<CrystalCrashers> implements JoinableState<CrystalCrashers, CrystalPlayer> {

    private Timer timer;

    @Getter
    private GameListener listener;

    @Override
    public long getLength() {
        return GameState.getLength(20 * 60 + 15, TimeUnit.SECONDS);
    }

    @Override
    public void onStart(CrystalCrashers game, Timer timer) {
        this.timer = timer;
        game.setCrystal((EnderCrystal) Bukkit.getWorld("world").spawnEntity(Bukkit.getWorld("world").getSpawnLocation(), EntityType.ENDER_CRYSTAL));
        game.getCrystal().setCustomNameVisible(true);
        game.getCrystal().setCustomName("§cHealth§8: §a" + game.getCrystalHealth());
        Bukkit.getServer().getPluginManager().registerEvents(listener = new GameListener(game), SparkCrashers.getInstance());
    }

    @Override
    public void onTick(CrystalCrashers game, Timer timer) {

    }

    @Override
    public void onEnd(CrystalCrashers game, Timer timer) {
        HandlerList.unregisterAll(listener);
    }

    @Override
    public boolean isJoinable(Player player, CrystalCrashers game, Timer timer) {
        return false;
    }

    @Override
    public void onJoin(Player player, CrystalCrashers game, Timer timer, CrystalPlayer gamePlayer) {

    }

    @Override
    public void onLeave(Player player, CrystalCrashers game, Timer timer, CrystalPlayer gamePlayer) {

    }
}
