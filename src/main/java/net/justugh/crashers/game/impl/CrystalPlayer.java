package net.justugh.crashers.game.impl;

import lombok.Getter;
import net.justugh.crashers.player.UndefinedPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class CrystalPlayer implements UndefinedPlayer {

    @Getter
    private CrystalCrashers game;
    private UUID uuid;

    @Getter
    private String name;

    public CrystalPlayer(CrystalCrashers game, Player player) {
        this.game = game;
        this.uuid = player.getUniqueId();
        this.name = player.getName();
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

}
