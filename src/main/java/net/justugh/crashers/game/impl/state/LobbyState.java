package net.justugh.crashers.game.impl.state;

import net.justugh.crashers.game.impl.CrystalCrashers;
import net.justugh.crashers.game.impl.CrystalPlayer;
import net.justugh.crashers.state.GameState;
import net.justugh.crashers.state.JoinableState;
import net.justugh.crashers.timer.Timer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class LobbyState extends GameState<CrystalCrashers> implements JoinableState<CrystalCrashers, CrystalPlayer> {

    private boolean infinite = true;

    public LobbyState() {
        setIgnoreFirstTick(false);
    }

    @Override
    public long getLength() {
        return GameState.getLength(30, TimeUnit.SECONDS);
    }

    @Override
    public boolean isInfinite() {
        return this.infinite;
    }

    @Override
    public void onStart(CrystalCrashers game, Timer timer) {

    }

    @Override
    public void onTick(CrystalCrashers game, Timer timer) {
        if (game.getPlayers().size() >= game.getMinPlayers()) {
            if (this.infinite) {
                this.infinite = false;
            }
        } else {
            if (!this.infinite) {
                this.infinite = true;
                timer.resetCurrentTime();
                Bukkit.broadcastMessage("§cNot enough players! Stopping countdown.");
                return;
            }
        }

        if (this.infinite)
            return;

        if (timer.getRemainingTime() > 0 && (timer.getRemainingTime() % 30 == 0 || timer.getRemainingTime() <= 5)) {
            Bukkit.broadcastMessage("The game starts in §a" + timer.getRemainingTime() + " §fsecond(s)!");
            Collection<Player> online = game.getOnlinePlayers();
            online.forEach(p -> p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 5, 1));
        }
    }

    @Override
    public void onEnd(CrystalCrashers game, Timer timer) {

    }

    @Override
    public void onJoin(Player player, CrystalCrashers game, Timer timer, CrystalPlayer gamePlayer) {
        String message = "§b" + player.getName() + " §fjoined! §e(" + game.getPlayers().size() + "/" + game.getMaxPlayers() + ")";
        Bukkit.broadcastMessage(message);
    }

    @Override
    public void onLeave(Player player, CrystalCrashers game, Timer timer, CrystalPlayer gamePlayer) {
        game.getPlayers().remove(gamePlayer);
    }

}
