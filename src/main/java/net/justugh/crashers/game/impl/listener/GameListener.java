package net.justugh.crashers.game.impl.listener;

import net.justugh.crashers.game.impl.CrystalCrashers;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class GameListener implements Listener {

    private CrystalCrashers game;

    public GameListener(CrystalCrashers game) {
        this.game = game;
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if(event.getEntity().getType() != EntityType.ENDER_CRYSTAL) {
            return;
        }

        if(game.getCrystalHealth() - 1 == 0) {
            event.getEntity().getLocation().getWorld().createExplosion(event.getEntity().getLocation(), 5f, true, false);
            event.getEntity().remove();
            game.setCrystalHealth(0);
            return;
        }

        game.setCrystalHealth(game.getCrystalHealth() - 1);
        event.getEntity().setCustomName("§cHealth§8: §a" + game.getCrystalHealth());
        event.setCancelled(true);
    }

}
