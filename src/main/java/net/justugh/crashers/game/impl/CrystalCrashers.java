package net.justugh.crashers.game.impl;

import lombok.Getter;
import lombok.Setter;
import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.game.impl.state.EndState;
import net.justugh.crashers.game.impl.state.InGameState;
import net.justugh.crashers.game.impl.state.LobbyState;
import net.justugh.crashers.state.GameTask;
import net.justugh.crashers.timer.Timer;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@Getter
@Setter
public class CrystalCrashers extends SparkGame<CrystalCrashers, CrystalPlayer> {

    private EnderCrystal crystal;
    private int crystalHealth = 200;

    public CrystalCrashers(Plugin plugin) {
        super(plugin);
        setMinPlayers(1);
        setMaxPlayers(12);

        addGameState(new LobbyState());
        addGameState(new InGameState());
        addGameState(new EndState());

        addBackgroundTask(new GameTask<CrystalCrashers>() {
            @Override
            public void onTick(CrystalCrashers game, Timer<CrystalCrashers> timer) {
                if(!(timer.getCurrentState() instanceof InGameState)) {
                    return;
                }

                if(game.getCrystalHealth() > 0) {
                    return;
                }

                timer.switchState(timer.getStateIndex() + 1);
            }
        });
    }

    @Override
    public CrystalPlayer constructPlayer(Player player) {
        return new CrystalPlayer(this, player);
    }

    @Override
    public void onJoin(Player player, Timer timer, CrystalPlayer gamePlayer) {
        super.onJoin(player, timer, gamePlayer);
    }

}
