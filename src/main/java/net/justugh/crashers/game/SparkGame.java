package net.justugh.crashers.game;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import net.justugh.crashers.player.UndefinedPlayer;
import net.justugh.crashers.state.GameState;
import net.justugh.crashers.state.GameTask;
import net.justugh.crashers.timer.Timer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Getter
public abstract class SparkGame<T extends SparkGame<T, ?>, E extends UndefinedPlayer> {

    private Plugin plugin;

    @Setter
    private int maxPlayers;
    @Setter
    private int minPlayers;

    private final List<GameState<T>> states = Lists.newArrayList();
    private final List<GameTask<T>> tasks = Lists.newArrayList();
    private final Map<UUID, E> players = Maps.newConcurrentMap();

    public SparkGame(Plugin plugin) {
        this.plugin = plugin;
    }

    public boolean addGameState(GameState<T> state) {
        return this.states.add(state);
    }

    public boolean addBackgroundTask(GameTask<T> state) {
        return this.tasks.add(state);
    }

    public Collection<E> getPlayers() {
        return players.values();
    }

    public Collection<Player> getOnlinePlayers() {
        List<Player> online = Lists.newArrayList();
        for (E player : getPlayers()) {
            Player p = Bukkit.getServer().getPlayer(player.getUUID());
            if (p != null)
                online.add(p);
        }

        return online;
    }

    public E getPlayer(UUID uuid) {
        return players.getOrDefault(uuid, null);
    }

    public E addPlayer(Player player) {
        E p = constructPlayer(player);
        if (p != null) {
            this.players.put(player.getUniqueId(), p);
            return p;
        }

        return null;
    }

    public abstract E constructPlayer(Player player);

    public boolean isJoinable(@Nullable Player player, Timer timer) {
        return false;
    }

    public void onJoin(Player player, Timer timer, E gamePlayer) {

    }

    public void onLeave(Player player, Timer timer, E gamePlayer) {

    }

}
