package net.justugh.crashers.game;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.justugh.crashers.player.UndefinedPlayer;
import net.justugh.crashers.state.JoinableState;
import net.justugh.crashers.timer.Timer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;

@Getter
public class GameManager<T extends SparkGame<T, E>, E extends UndefinedPlayer> {

    private Plugin plugin;
    private List<GameHandler<T>> games = Lists.newArrayList();

    public GameManager(Plugin plugin) {
        this.plugin = plugin;
    }

    public void addGame(T game) {
        this.games.add(new GameHandler<>(game, new Timer<>(game, plugin)));
    }

    public T getGame(Player player) {
        for(GameHandler<T> handler : games) {
            if(handler.getGame().getPlayer(player.getUniqueId()) == null) {
                continue;
            }

            return handler.getGame();
        }

        return null;
    }

    public List<T> getJoinableGames() {
        List<T> games = Lists.newArrayList();

        for(GameHandler<T> handler : this.games) {
            if(handler.getTimer().getCurrentState() == null || !(handler.getTimer().getCurrentState() instanceof JoinableState)) {
                continue;
            }

            JoinableState<T, E> state = (JoinableState<T, E>) handler.getTimer().getCurrentState();

            if(!state.isJoinable(null, handler.getGame(), handler.getTimer())) {
                continue;
            }

            games.add(handler.getGame());
        }

        return games;
    }

    public GameHandler<T> getHandler(T game) {
        for(GameHandler<T> handler : games) {
            if(handler.getGame() == game) {
                return handler;
            }
        }

        return null;
    }

    public void joinGame(Player player, T game, boolean force) {
        GameHandler<T> handler = getHandler(game);

        if(handler == null || handler.getTimer().getCurrentState() == null) {
            return;
        }

        if(handler.getTimer().getCurrentState() instanceof JoinableState) {
            if(force || game.isJoinable(player, handler.getTimer())) {
                game.onJoin(player, handler.getTimer(), game.addPlayer(player));
                return;
            }
        }

        JoinableState<T, E> joinState = (JoinableState<T, E>) handler.getTimer().getCurrentState();

        if (force || joinState.isJoinable(player, game, handler.getTimer())) {
            joinState.onJoin(player, game, handler.getTimer(), game.addPlayer(player));
        }
    }

    public void leaveGame(Player player, T game) {
        GameHandler<T> handler = getHandler(game);

        if(handler == null || handler.getTimer().getCurrentState() == null) {
            return;
        }

        if(handler.getTimer().getCurrentState() instanceof JoinableState) {
            JoinableState<T, E> state = (JoinableState<T, E>) handler.getTimer().getCurrentState();
            state.onLeave(player, game, handler.getTimer(), game.getPlayer(player.getUniqueId()));
            return;
        }

        game.onLeave(player, handler.getTimer(), game.getPlayer(player.getUniqueId()));
    }
}
