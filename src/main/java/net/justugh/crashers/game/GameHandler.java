package net.justugh.crashers.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.justugh.crashers.timer.Timer;

@AllArgsConstructor
@Getter
public class GameHandler<T extends SparkGame<?, ?>> {

    private T game;
    private Timer timer;

}
