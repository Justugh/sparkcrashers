package net.justugh.crashers.player;

import java.util.UUID;

public interface UndefinedPlayer {

    UUID getUUID();
}
