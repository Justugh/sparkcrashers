package net.justugh.crashers.state;

import lombok.Setter;
import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.timer.Timer;

public abstract class GameTask<T extends SparkGame<T, ?>> implements Tickable<T> {

    @Setter
    private boolean alwaysRunning = true;

    public abstract void onTick(T game, Timer<T> timer);
}
