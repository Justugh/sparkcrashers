package net.justugh.crashers.state;

import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.player.UndefinedPlayer;
import net.justugh.crashers.timer.Timer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public interface JoinableState<T extends SparkGame<T, E>, E extends UndefinedPlayer> {

    default boolean isJoinable(@Nullable Player player, T game, Timer timer) {
        return true;
    }

    void onJoin(Player player, T game, Timer timer, E gamePlayer);

    void onLeave(Player player, T game, Timer timer, E gamePlayer);
}
