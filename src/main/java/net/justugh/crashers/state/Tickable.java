package net.justugh.crashers.state;

import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.timer.Timer;

public interface Tickable<T extends SparkGame<T, ?>> {

    void onTick(T game, Timer<T> timer);
}
