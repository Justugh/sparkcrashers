package net.justugh.crashers.state;

import lombok.Getter;
import lombok.Setter;
import net.justugh.crashers.game.SparkGame;
import net.justugh.crashers.timer.Timer;

import java.util.concurrent.TimeUnit;

@Getter
@Setter
public abstract class GameState<T extends SparkGame<T, ?>> implements Tickable<T> {

    private boolean ignoreFirstTick;

    public boolean isInfinite() {
        return false;
    }

    public abstract long getLength();

    public abstract void onStart(T game, Timer<T> timer);

    public abstract void onTick(T game, Timer<T> timer);

    public abstract void onEnd(T game, Timer<T> timer);

    public static long getLength(long time, TimeUnit unit) {
        return TimeUnit.SECONDS.convert(time, unit);
    }
}
