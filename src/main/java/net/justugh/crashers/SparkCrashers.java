package net.justugh.crashers;

import net.justugh.crashers.game.GameManager;
import net.justugh.crashers.game.impl.CrystalCrashers;
import net.justugh.crashers.game.impl.CrystalPlayer;
import net.justugh.crashers.listener.LoginListener;
import org.bukkit.plugin.java.JavaPlugin;

public class SparkCrashers extends JavaPlugin {

    private static SparkCrashers instance;

    private GameManager<CrystalCrashers, CrystalPlayer> gameManager;

    @Override
    public void onEnable() {
        instance = this;

        gameManager = new GameManager<>(this);
        CrystalCrashers game = new CrystalCrashers(this);
        gameManager.addGame(game);
        getServer().getPluginManager().registerEvents(new LoginListener(gameManager, this), this);
    }

    @Override
    public void onDisable() {

    }

    public static SparkCrashers getInstance() {
        return instance;
    }

}
