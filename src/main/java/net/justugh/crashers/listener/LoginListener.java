package net.justugh.crashers.listener;

import lombok.Getter;
import lombok.Setter;
import net.justugh.crashers.game.GameManager;
import net.justugh.crashers.game.SparkGame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class LoginListener implements Listener {

    private GameManager gameManager;

    @Getter
    @Setter
    private boolean kickIfFull = true;

    public LoginListener(GameManager gameManager, Plugin plugin) {
        this.gameManager = gameManager;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        event.setJoinMessage("");
        List<SparkGame> joinableGames = gameManager.getJoinableGames();

        if(joinableGames.size() >= 1) {
            gameManager.joinGame(player, joinableGames.get(0), false);
        } else if(kickIfFull) {
            player.kickPlayer("§cNo games available!");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage("");
        SparkGame game = gameManager.getGame(player);

        if(game != null) {
            gameManager.leaveGame(player, game);
        }
    }
}
